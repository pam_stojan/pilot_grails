package com.edis.domain

import grails.converters.JSON

/**
 * Controller for facilitating tender requests
 *
 */
class TenderController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def mailService
	def tenderToXmlService

    def index = {
        redirect(action: "create", params: params)
    }

    def create = {
        def tenderInstance = new Tender()
        tenderInstance.properties = params
        return [tenderInstance: tenderInstance]
    }

    def save = {
		def tenderInstance = new Tender(params)
		
		//create email
		def mailContent = message(code: "mail.content")
		def mailSubject = message(code:"mail.subject")
		
        if (tenderInstance.save(flush: true)  ) {
			//send mail with link
			def saveLink =  createLink(controller:'tender', action:'saveXml',params: [tender_id:tenderInstance.id] ,absolute:true)
			mailContent +="\n"+saveLink.toString()
			def mail = new Email(reciever:tenderInstance.contactEmail, content:mailContent, subject:mailSubject)
			mailService.send(mail)
			
            redirect(action: "show", id: tenderInstance.id)
        }
        else {
            render(view: "create", model: [tenderInstance: tenderInstance])
        }
    }
	
	def saveXml = {
		def tenderInstance = Tender.get(params.tender_id)
		
		if(tenderToXmlService.save(tenderInstance))
		{
			render("Thank you for confirming tender")
		}
		else{
			render("Error! Tender not confirmed. Please try again later")
		}
	}

    def show = {
        def tenderInstance = Tender.get(params.id)
        flash.message = "Mail sent. You will get an email with a conformation link"
    }

	def autocompleteNuts = {
		def nuts = Nuts.findAllByNameLikeOrIdLike("%${params.term}%", "%${params.term}%", [max:20])
		render nuts*.toString() as JSON
	}
	
    def autocompleteCPV = {
    	def cpv = CPVCode.findAllByNameLikeOrIdLike("%${params.term}%", "%${params.term}%", [max:20])
    			render cpv*.toString() as JSON
    }
}
