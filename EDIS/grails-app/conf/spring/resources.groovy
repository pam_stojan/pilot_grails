import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

beans = {
	customPropertyEditorRegistrar(com.edis.util.CustomDateEditorRegistrar)
	mailSender(JavaMailSenderImpl) {
		host = 'smtp.pamet.co.rs'
	 }
	mailMessage(SimpleMailMessage) {
		from = 'aleksandra.manic@pamet.co.rs'
	 }
}
