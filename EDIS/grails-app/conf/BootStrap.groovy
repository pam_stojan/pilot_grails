class BootStrap {

	def dataLoaderService
	
    def init = { servletContext ->
		
		dataLoaderService.loadNutsCodes()
		dataLoaderService.loadCPVCodes()
    }
    def destroy = {
    }
}
