package com.edis

import org.springframework.mail.MailException
import org.springframework.mail.MailSender
import org.springframework.mail.SimpleMailMessage

class MailService {

    static transactional = true
	MailSender mailSender
	SimpleMailMessage mailMessage

    boolean send(mail) {
		
		SimpleMailMessage message = new SimpleMailMessage(mailMessage)
		message.to = [mail.reciever]
		message.text = mail.content
		message.subject = mail.subject
		
		try {
			mailSender.send(message as SimpleMailMessage)
			return true
		} catch (MailException ex){
			//log err
			return false
		}
    }
}
