package com.edis

import org.codehaus.groovy.grails.commons.ConfigurationHolder;

import javax.xml.XMLConstants
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory

import org.w3c.tidy.Tidy
import org.xml.sax.SAXException;

import groovy.xml.MarkupBuilder

class TenderToXmlService {

	static transactional = true
	
	def grailsApplication

	/**
	 * Saves tender in XML file validated against schema.
	 * @param tender to save
	 * @return true if file is saved, false otherwise
	 */
	boolean save(tender) {
		def tenderXml = getXml(tender)
		try {
			//validating file against schema
			def schemaFile = getSchemaFile()
			def fullFileLocationPath = getTendersLocationPath()
			validate(schemaFile, tenderXml)
			//saving tender xml file 
			def xmlFile = new File(fullFileLocationPath+ "/" + tender.createFileName())
			FileWriter fstream = new FileWriter(xmlFile);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(tenderXml);
			out.close();
			return true
		} catch(SAXException ex) {
			return false
			//log err
		} catch(IOException ex) {
			return false
			//log err
		}
	}

	

	/**
	 * returns XML representation for given tender object
	 * @param tender - to be serialized to XML
	 * @return
	 */
	private String getXml(tender) {
		def writer = new StringWriter()
		def xml = new MarkupBuilder(writer)
		//sanja todo add xml declaration
		xml.tender(xmlns:"http://www.tender-service.com/apollo"){
			externalId (tender.externalId)
			nutsCodes(tender.region)
			natureOfContract(tender.typeOfContract)
			publicationDate(tender.publicationDate.format("yyyy-MM-dd'T'HH:mm:ss"))
			typeOfDocument(tender.typeOfDocument)
			tenderContents{
				tenderContent{
					title(tender.title)
					description(tender.description)
					fulltext(tender.fullText)
					//sanja todo: clean text with tidy
					fulltextCleaned(clean(tender.fullText))
					//sanja todo insert language here
					language 'en'
				}
			}
			cpvCodes(tender.cpvCode)
			deadlineDate(tender.deadlineDate.format("yyyy-MM-dd'T'HH:mm:ss"))
		}

		return writer.toString()
	}
	
	/**
	 * Validates XML against given schema 
	 * @param schemaLocation- location of schema to validate against
	 * @param xml - for validation 
	 * @return
	 */
	private String validate(schemaFile, xml){
		def factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
		def schema = factory.newSchema(new StreamSource(schemaFile))
		def validator = schema.newValidator()
		validator.validate(new StreamSource(new StringReader(xml)))
	}

	/**
	 * Cleans given text with jtidy
	 * @param text to clean
	 * @return text cleaned with tidy
	 */
	private String clean(text) {
		Tidy tidy = new Tidy()
		StringWriter writer = new StringWriter()
		tidy.identity {
			setXmlTags(false)//working with html not xml as input
			setQuiet(true) //don't show warnings
			parse(new StringReader(text), writer)
		}
		writer.toString()
	}
	
	/**
	 * Gets schema location and returns schema file
	 * @return
	 */
	private String getSchemaFile() {
		def schemaLocation = getSchemaLocation()
		def schemaFile = grailsApplication.parentContext.getResource(schemaLocation).file
		return schemaFile
	}
	
	/**
	 * Gets full path to folder where tender will be saved
	 * @return
	 */
	private String getTendersLocationPath() {
		def fileLocation = getFileLocation()
		def fullFileLocationPath = grailsApplication.parentContext.getResource(fileLocation).file.toString()
		return fullFileLocationPath
	}


	/**
	 * gets configured tenders file location
	 * @return
	 */
	private String getFileLocation() {
		def fileLocation = ConfigurationHolder.config.edis.tender.fileLocation
		return fileLocation
	}

	/**
	 * gets confiured schema file location
	 * @return
	 */
	private String getSchemaLocation() {
		def schemaLocation= ConfigurationHolder.config.edis.tender.schemaLocation
		return schemaLocation
	}
}
