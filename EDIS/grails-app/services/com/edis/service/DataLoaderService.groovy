package com.edis.service

import com.edis.domain.CPVCode
import com.edis.domain.Nuts
import org.springframework.context.ResourceLoaderAware
import org.springframework.core.io.ResourceLoader

class DataLoaderService implements ResourceLoaderAware {
	
	static transactional = true
	
	def resourceLoader
	
	protected final String NUTS_CODES_FILE = "nuts.groovy"
	protected final String CPV_CODES_FILE = "cpv.groovy"


    def loadNutsCodes() {
		
		for (List nutsItem in getNutsCodesFromFile(NUTS_CODES_FILE)) {
			def nutsArray = nutsItem.toArray()
			if (nutsArray.length >= 2) {
				Nuts nuts = new Nuts((String) nutsArray[0], (String) nutsArray[1])
				nuts.save()
			}
		}
    }
	
	def loadCPVCodes() {
		
		for (List cpvItem in getCPVCodesFromFile(CPV_CODES_FILE)) {
			def cpvArray = cpvItem.toArray()
					if (cpvArray.length >= 2) {
						CPVCode cpv = new CPVCode((String) cpvArray[0], (String) cpvArray[1])
						cpv.save()
					}
		}
	}
	
	private getNutsCodesFromFile(String fileName) {
		getContentFromFile("/WEB-INF/data/${fileName}")
	}
	
	private getCPVCodesFromFile(String fileName) {
		getContentFromFile("/WEB-INF/data/${fileName}")
	}
	
	private getContentFromFile(String filePath) {
		GroovyShell shell = new GroovyShell()
		try {
			def text = resourceLoader.getResource(filePath).inputStream.getText("UTF-8")
			return shell.evaluate(text)
		} catch (Exception e) {
			log.error "Evaluation of file ${filePath} failed", e
			return []
		}
	}
	
	public void setResourceLoader(ResourceLoader rl) {
		this.resourceLoader = rl
	}
	
	
}
