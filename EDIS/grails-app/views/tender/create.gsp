
<%@ page import="com.edis.domain.Tender" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />	
        <title><g:message code="tender.create" default="Create Tender" /></title>
        <g:javascript library="jquery" plugin="jquery"/>
        <jqui:resources/>
        <jqval:resources />
        <jqvalui:resources />
        <jqvalui:renderValidationScript for="com.edis.domain.Tender" />
		<gui:resources components="['richEditor']"/>
		<g:javascript src="combobox.js" />
        <script type="text/javascript">
	        $(document).ready(function()
	        {
				$("#deadlineDate").datepicker({dateFormat: 'dd/mm/yy'});
				$("#publicationDate").datepicker({dateFormat: 'dd/mm/yy'});
				$("#tomorrow").click(function(){
				 	var tomorrow = new Date();
				 	tomorrow.setDate(tomorrow.getDate() + 1);
				$("#publicationDate").val($.datepicker.formatDate( 'dd/mm/yy', tomorrow));		      	    	
				      });
				      
				$("#deadlineDateChkbox").change(function(){
				var chk = $(this);
				if (chk.is (':checked')) {
					$("#deadlineDate").attr("disabled", "disabled"); 
				} else {
					$("#deadlineDate").removeAttr("disabled");
				}
				      });
				      
				$("#region").autocomplete({source: '${createLink(action: 'autocompleteNuts')}'});
				$("#cpvCode").autocomplete({source: '${createLink(action: 'autocompleteCPV')}'});
				
				$(".required").change(function() {
					var empty = false;
			        $(".required").each(function() {
			            if ($(this).val() == '') {
			                empty = true;
			            }
			        });

			        if (empty) {
			            $('#create').attr('disabled', true);
			        } else {
			            $('#create').attr('disabled', false);
			        }
								
				})
	          
	        })
    	</script>		
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
        </div>
        <div class="body">
            <h1><g:message code="tender.create" default="Create Tender" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${tenderInstance}">
            <div class="errors">
                <g:renderErrors bean="${tenderInstance}" as="list" />
            </div>
            </g:hasErrors>
            <%-- client side validation--%>
            <jqvalui:renderErrors render="true" style="margin-bottom:10px"/>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="title"><g:message code="tender.title" default="Title" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'title', 'errors')}">
                                    <g:textField class="required" name="title" value="${tenderInstance?.title}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="description"><g:message code="tender.description" default="Description" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'description', 'errors')}">
                                    <g:textArea name="description" value="${fieldValue(bean: tenderInstance, field: 'description')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fullText"><g:message code="tender.fullText" default="Full Text" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'fullText', 'errors')}">
                                    <gui:richEditor class="required" id="fullText" value="${fieldValue(bean: tenderInstance, field: 'fullText')}" height="200px" width="558px"/>
								    <script type="text/javascript">GRAILSUI.fullText.defaultToolbar.titlebar = 'Enter your full text';</script>
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="region"><g:message code="tender.region" default="Region" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'region', 'errors')}">
                                    <g:textField id="region" class="required" name="region" value="${fieldValue(bean: tenderInstance, field: 'region')}" style="width:200px;" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="cpvCode"><g:message code="tender.cpvCode" default="Cpv Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'cpvCode', 'errors')}">
                                    <g:textField name="cpvCode" value="${fieldValue(bean: tenderInstance, field: 'cpvCode')}" style="width:200px;"/>

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="typeOfContract"><g:message code="tender.typeOfContract" default="Type Of Contract" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'typeOfContract', 'errors')}">
                                    <g:select class="required" name="typeOfContract" from="${com.edis.data.TypeOfContract?.values()*.name}" keys="${com.edis.data.TypeOfContract?.values()}" value="${tenderInstance?.typeOfContract?.name}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="typeOfDocument"><g:message code="tender.typeOfDocument" default="Type Of Document" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'typeOfDocument', 'errors')}">
                                    <g:select class="required" name="typeOfDocument" from="${com.edis.data.TypeOfDocument?.values()*.name}" keys="${com.edis.data.TypeOfDocument?.values()}" value="${tenderInstance?.typeOfDocument?.name}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="authorityName"><g:message code="tender.authorityName.label" default="Authority Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'authorityName', 'errors')}">
                                    <g:textField class="required" name="authorityName" value="${tenderInstance?.authorityName}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="contactName"><g:message code="tender.contactName.label" default="Contact Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'contactName', 'errors')}">
                                    <g:textField class="required" name="contactName" value="${tenderInstance?.contactName}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="contactTel"><g:message code="tender.contactTel.label" default="Contact Tel" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'contactTel', 'errors')}">
                                    <g:textField class="required" name="contactTel" value="${tenderInstance?.contactTel}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="contactEmail"><g:message code="tender.contactEmail.label" default="Contact Email" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'contactEmail', 'errors')}">
                                    <g:textField class="required" name="contactEmail" value="${tenderInstance?.contactEmail}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="publicationDate"><g:message code="tender.publicationDate" default="Publication Date" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'publicationDate', 'errors')}">
                                    <g:textField class="required" id="publicationDate" name="publicationDate" value="${tenderInstance?.publicationDate}" style="width:100px;" />
									<input id="tomorrow" type="button" value="Tomorrow">
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="deadlineDate"><g:message code="tender.deadlineDate" default="Deadline Date" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'deadlineDate', 'errors')}">
                                    <g:textField id="deadlineDate" name="deadlineDate" value="${tenderInstance?.deadlineDate}" style="width:100px;"/>
                                    <g:checkBox id="deadlineDateChkbox" name="deadlineDateChkbox"/>
                                    <label for="deadlineDateChkbox"><g:message code="tender.deadlineDateChkbox" default="No termination date / termination date unknown" /></label>
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="externalId"><g:message code="tender.externalId" default="External Id" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tenderInstance, field: 'externalId', 'errors')}">
                                    <g:textField class="required" name="externalId" maxlength="100" value="${fieldValue(bean: tenderInstance, field: 'externalId')}" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="buttons" style="width:150px;">
                    <span class="button"><g:submitButton id="create" name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" disabled="disabled" style="width: 150px"/></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
