package com.edis.domain

/**
 * Domain class representing CPV code
 *
 */
class CPVCode {

    public CPVCode(String id = "", String name = null) {
		this.id = id
		this.name = name
	}

	String name
	String id
	
	static mapping = {
		version false
		id generator: 'assigned', name: 'id', type: 'string'
	}

	static constraints = {
		name(size: 0..100, blank: true, nullable: true)
		id(size: 1..12, blank: false)
	}
	
	String toString() {
		return "${id} - ${name}"
	}
}
