package com.edis.domain

import com.edis.data.TypeOfContract;
import com.edis.data.TypeOfDocument;

/**
 *  Domain class representing tender data
 */
class Tender {
	
	String title
	String description
	String fullText
	String region
	String cpvCode
	TypeOfContract typeOfContract
	TypeOfDocument typeOfDocument
	String authorityName
	String contactName
	String contactTel
	String contactEmail
	Date publicationDate
	Date deadlineDate
	String externalId
	
	Date dateCreated
	

    static constraints = {
		title(size:1..1000, blank:false)
		description(size:0..4, nullable:true)
		fullText(blank:false)
		region(size:2..10000, blank:false)
		cpvCode(size:0..10000, nullable:true)
		typeOfContract(blank:false)
		typeOfDocument(blank:false)
		authorityName(blank:false)
		contactName(blank:false)
		contactTel(blank:false)
		contactEmail(email:true, blank:false)
		publicationDate(blank:false)
		deadlineDate(nullable:false)
		externalId(size:0..100, blank:false)
		
    }
	
	String createFileName(){
		return "tender_${title}_${id}.xml"
	}
	
}
