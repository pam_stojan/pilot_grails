package com.edis.data

public enum TypeOfDocument {
	// TODO Internationalization
	TENDER('Tender'),
	PRIOR_INFORMATION('Prior Information'),
	CORRECTION_CANCELLATION('Correction cancellation'),
	RESULT('Result'),
	OTHER_INFORMATION('Other information')
	
	TypeOfDocument(String name) {
		this.name = name
	}
	
	private String name
	
	public getName() {
		return name
	}
		
}
