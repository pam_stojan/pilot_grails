package com.edis.data

public enum TypeOfContract {
	// TODO Internationalization
    PUBLIC_WORKS_CONTRACT('Public works contract'),
    SUPPLY_CONTRACT('Supply contract'),
    COMBINED_CONTRACT('Combined contract'),
    SERVICE_CONTRACT('Service contract'),
    OTHER('Other')
	
	TypeOfContract(String name) {
		this.name = name;
	}		
	
	private String name
	
	public getName() {
		return name
	}
}
